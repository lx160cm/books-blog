# Dockerfile, Image, Container
FROM python
#WORKDIR

COPY requirements.txt .
RUN pip install -r requirements.txt
# COPY ./app ./app
CMD ["python", "./main.py"]
